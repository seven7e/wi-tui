use cursive::{Vec2, XY, Printer, theme::{ColorStyle, Color, BaseColor}, event::{Event, EventResult, MouseEvent, Key}, Cursive, views::{Dialog}};

type Cell = char;

struct EditorView {
    size: XY<usize>,
    overlay: Vec<Cell>,
    buffer: Vec<Vec<Cell>>,
    focused: Option<Vec2>,
    cursor: XY<usize>,
}

struct EditorViewOptions {
    size: XY<usize>,
}

impl EditorView {
    pub fn new(opts: &EditorViewOptions) -> Self {
        let overlay = vec!['\0'; 100];
        EditorView {
            size: opts.size,
            overlay,
            buffer: vec![vec![' '; 10]; 10],
            focused: None,
            cursor: XY::new(0, 0),
        }
    }

    fn forward_cursor(&mut self, ofst: usize) {
        self.cursor.x += ofst;
        while self.cursor.x >= self.size.x {
            if self.cursor.y + 1 >= self.size.y {
                self.cursor.x = self.size.x - 1;
                return;
            }
            self.cursor.x -= self.size.x;
            self.cursor.y += 1;
        }
    }

    fn backward_cursor(&mut self, ofst: usize) {
        if self.cursor.x <= ofst && self.cursor.y <= 0 {
            self.cursor.x = 0;
            self.cursor.y = 0;
            return;
        }

        let mut n = ofst;
        while n > self.cursor.x {
            if self.cursor.y <= 0 {
                self.cursor.x = 0;
                return;
            }
            if n < self.size.x {
                self.cursor.x += self.size.x - n;
                self.cursor.y -= 1;
                return;
            }
            n -= self.size.x;
            self.cursor.y -= 1;
        }
        self.cursor.x -= n;
    }

    fn set_at_cursor(&mut self, v: Cell) {
        self.set_buffer(self.cursor.x, self.cursor.y, v);
    }

    fn get_at_cursor(&self) -> Cell {
        self.get_buffer(self.cursor.x, self.cursor.y).unwrap_or(' ')
    }

    fn append_at_cursor(&mut self, v: Cell) -> &mut Self{
        self.set_at_cursor(v);
        self.forward_cursor(1);
        self
    }

    fn backspace_at_cursor(&mut self) -> &mut Self {
        self.backward_cursor(1);
        self.set_at_cursor(' ');
        self
    }

    fn set_buffer(&mut self, x: usize, y: usize, v: Cell) -> &mut Self {
        if y >= self.buffer.len() {
            self.buffer.resize(y + 1, vec![]);
        }
        let row = &mut self.buffer[y];
        if x >= row.len() {
            row.resize(x + 1, ' ');
        }
        self.buffer[y][x] = v;
        self
    }

    fn get_buffer(&self, x: usize, y: usize) -> Option<Cell> {
        if x >= self.size.x || y >= self.size.y {
            return None;
        }
        if y >= self.buffer.len() || x >= self.buffer[y].len() {
            return None;
        }

        Some(self.buffer[y][x])
    }

    fn change_buffer(&mut self) -> &mut Self {
        // println!("change!");
        let width = if self.size.x <= 10 {
           8
        } else {
            self.size.x / 2 - 5
        };
        // println!("size: {} * {}", self.size.x, self.size.y);
        // println!("buffer size: {} * {}", self.buffer.len(), self.size.y);
        // println!("{}", width);

        for i in 0..self.overlay.len() {
            let x = (i % width) * 2;
            let y = i / width;
            self.set_buffer(x, y, 'z');
        }
        self
    }
}

impl cursive::view::View for EditorView {
    fn draw(&self, printer: &Printer) {
        // println!("draw!");
        let color = Color::RgbLowRes(5, 2, 2);
        for y in 0..self.buffer.len() {
        // for y in 0..1 {
        // {
            // let y = 1;
            if self.buffer[y].len() > 0 {
                // println!("row1 {}, {} {}", self.buffer[y].len(), self.buffer[y][0], self.buffer[y][1]);
                // let s = self.buffer[y].iter().cloned().collect::<String>();
                let s = String::from_iter(self.buffer[y].iter());
                // let s = "\u{1111}\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a hello world".to_string();
                // let s = "abc";
                // for x in 1..self.size.x {
                // printer.with_color(
                    // ColorStyle::new(Color::Dark(BaseColor::Black), color),
                    // |printer| printer.print((0, y), s.as_str()),
                // );
                printer.print((0, y), s.as_str());
                // println!("{}", s.as_str());
            }

            printer.with_color(
                ColorStyle::new(Color::Dark(BaseColor::Black), color),
                |printer| printer.print((self.cursor.x, self.cursor.y), &self.get_at_cursor().to_string()),
            );
            // printer.print((12, 0), format!("{} x {}", &self.size.x, &self.size.y));
            // }
        }
        // println!("row1 {}, {} {}", self.buffer[0].len(), self.buffer[0][0], self.buffer[0][1]);
        // println!("row2 {}, {} {}", self.buffer[1].len(), self.buffer[1][0], self.buffer[1][1]);
        // println!("{}", String::from_iter(&self.buffer[0]));
        printer.with_color(
            ColorStyle::new(Color::Dark(BaseColor::Black), color),
            |printer| printer.print(XY::new(0, 12), "{} x {}"),
            );
        let s = format!("{} x {}", self.size.x, self.size.y);
        printer.print(XY::new(0, 13), &s);
    }

    fn layout(&mut self, size: Vec2) {
        // println!("layout: {} * {}", self.size.x, self.size.y);
        // self.change_buffer();
        self.size = size;
    }

    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        // println!("required size: {} * {}", self.size.x, self.size.y);
        // self.size.map_x(|x| 2 * x)
        // self.size = constraint;
        constraint
    }

    fn on_event(&mut self, event: Event) -> EventResult {
        // self.set_at_cursor('x');
        // return EventResult::Consumed(None);
        // println!("on-event {:?}", event);
        match event {
            Event::Mouse {
                offset,
                position,
                event: MouseEvent::Press(_btn),
            } => {
                // Get cell for position
                // if let Some(pos) = self.get_cell(position, offset) {
                //     self.focused = Some(pos);
                //     return EventResult::Consumed(None);
                // }
            }
            Event::Mouse {
                offset,
                position,
                event: MouseEvent::Release(btn),
            } => {
                // Get cell for position
                // if let Some(pos) = self.get_cell(position, offset) {
                //     if self.focused == Some(pos) {
                //         // We got a click here!
                //         match btn {
                //             MouseButton::Left => return self.reveal(pos),
                //             MouseButton::Right => {
                //                 self.flag(pos);
                //                 return EventResult::Consumed(None);
                //             }
                //             MouseButton::Middle => {
                //                 return self.auto_reveal(pos);
                //             }
                //             _ => (),
                //         }
                //     }

                //     self.focused = None;
                // }
            }
            Event::Key(key) => {
                match key {
                    Key::Backspace => {
                        self.backspace_at_cursor();
                    }
                    _ => (),
                }
            }
            Event::Char(c) => {
                self.append_at_cursor(c);
            }
            _ => (),
        }

        EventResult::Ignored
    }
}

fn new_game(siv: &mut Cursive) {
    // let _board = game::Board::new(options);
    let opts = EditorViewOptions {
        size: XY::new(10, 10)
    };

    let mut view = EditorView::new(&opts);
    view.change_buffer();

    // siv.add_layer(
    siv.add_fullscreen_layer(view);
    // siv.add_fullscreen_layer(
    //     Dialog::new()
    //         .title("Minesweeper")
    //         .button("Quit game", |s| {
    //             s.pop_layer();
    //         }),
    // );

    // siv.add_layer(Dialog::info(
    //     "Controls:
// Reveal cell:                  left click
// Mark as mine:                 right-click
// Reveal nearby unmarked cells: middle-click",
    // ));
}

pub fn run() {
    let mut siv = cursive::default();
    siv.load_toml(include_str!("../etc/style.toml")).unwrap();

    // siv.add_global_callback('r', restart);
    siv.add_global_callback('r', _debug);
    siv.add_global_callback('q', Cursive::quit);

    siv.set_fps(2);

    new_game(&mut siv);

    siv.run();
}

fn _debug(s: &mut Cursive) {
    // println!("screen size: {} x {}", s.screen_size().x, s.screen_size().y)
    s.add_layer(Dialog::info(
        format!("screen size: {} x {}", s.screen_size().x, s.screen_size().y)
            ))
}

